return {
  { "rebelot/kanagawa.nvim", name = "kanagawa", lazy= false },
  { "catppuccin/nvim", name = "catppuccin", lazy = false },
  { 'rose-pine/neovim', name = 'rose-pine', lazy = false },
  { 'yassinebridi/vim-purpura', name = 'purpura', lazy = false },
  { 'Shadorain/shadotheme', name = 'shadotheme', lazy = false },
  { 'neanias/everforest-nvim', name = 'everforest', lazy = false,
    version = false,
    config = function()
      require("everforest").setup({
       -- Your config here
        background = "hard",
        italic = true,
    })
    end,},
  { 'folke/tokyonight.nvim', name = 'tokyonight', lazy = false },
}
